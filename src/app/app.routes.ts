import { Routes } from '@angular/router';
import { CteComponent } from './cte/cte.component';
import { NfeComponent } from './nfe/nfe.component';
import { ConfiguracaoComponent } from './configuracao/configuracao.component';
import { CteConsultaComponent } from './cte-consulta/cte-consulta.component';

export const routes: Routes = [
    { path: 'cte', component: CteComponent },
    { path: 'nfe', component: NfeComponent },
    { path: 'configuracao', component: ConfiguracaoComponent },
    { path: 'consulta', component: CteConsultaComponent },
];

