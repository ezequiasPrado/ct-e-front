import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-configuracao',
  standalone: true,
  imports: [HttpClientModule, FormsModule],
  providers:[HttpClientModule, HttpClient, FormsModule],
  templateUrl: './configuracao.component.html',
  styleUrl: './configuracao.component.css'
})
export class ConfiguracaoComponent {

  obj = localStorage.getItem("config") != null ? JSON.parse(localStorage.getItem("config") || "") : {};

  config = { 
    certificado: this.obj.certificado != null ? this.obj.certificado : "",
    senha: this.obj.senha != null ? this.obj.senha : "", 
    schemasNFe: this.obj.schemasNFe != null ? this.obj.schemasNFe : "",
    schemasCTe: this.obj.schemasCTe != null ? this.obj.schemasCTe : ""
  }

  aplicar(config: any){  
    localStorage.setItem("config", JSON.stringify(config));
    alert("Configuração salva com sucesso.")
  }


}
