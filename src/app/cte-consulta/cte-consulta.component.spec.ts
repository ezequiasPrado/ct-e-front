import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CteConsultaComponent } from './cte-consulta.component';

describe('CteConsultaComponent', () => {
  let component: CteConsultaComponent;
  let fixture: ComponentFixture<CteConsultaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CteConsultaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CteConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
