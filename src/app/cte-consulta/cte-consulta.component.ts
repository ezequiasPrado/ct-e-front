import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-cte-consulta',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cte-consulta.component.html',
  styleUrl: './cte-consulta.component.css'
})
export class CteConsultaComponent {

  ctesRejeitado = (localStorage.getItem("cteRejeitado") != null) ? JSON.parse(localStorage.getItem("cteRejeitado")||"") : [];
  ctesAutorizado = (localStorage.getItem("cteAutorizado") != null) ? JSON.parse(localStorage.getItem("cteAutorizado")||"") : [];
  ctesCancelado = (localStorage.getItem("cteCancelado") != null) ? JSON.parse(localStorage.getItem("cteCancelado")||"") : [];

  nfesAutorizado = (localStorage.getItem("nfeAutorizado") != null) ? JSON.parse(localStorage.getItem("nfeAutorizado")||"") : [];
  nfesRejeitado = (localStorage.getItem("nfeRejeitado") != null) ? JSON.parse(localStorage.getItem("nfeRejeitado")||"") : [];
  nfesCancelado = (localStorage.getItem("nfeCancelado") != null) ? JSON.parse(localStorage.getItem("nfeCancelado")||"") : [];

  ngOnInit(): void{
 }

}
