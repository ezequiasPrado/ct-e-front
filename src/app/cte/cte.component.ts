import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';



@Component({
  selector: 'app-cte',
  standalone: true,
  imports: [HttpClientModule, FormsModule],
  providers:[HttpClientModule, HttpClient, FormsModule],
  templateUrl: './cte.component.html',
  styleUrl: './cte.component.css'
})

export class CteComponent {
  
  url = 'http://localhost:8080/api/cte';

  cte ={ cnpj: "32695077000110", modelo: "57", tipoEmissao:"1", cfop:"5351", tpImp:"1", natOp:"TRANSPORTE INTERNO", numero:"", serie:"1", tpCTe:"0", xMunEnv:"Criciúma", 
  modal:"01", tpServ:"0", cMunEnv:"4204608", ufEnv:"SC", retira:"0", indIEToma:"9", cMunIni:"4204608", ufIni:"SC", xMunIni:"Criciúma", xMunFim:"Criciúma",
  cMunFim:"4204608", ufFim:"SC", toma:"3", xObs:"Sem valor fiscal", emitCnpj:"32695077000110", emitInscricaoEstadual:"258966696", emitXNome:"TRC TRANSPORTES LTDA", crt:"1", emitFone:"6233215175",
  emitCep:"72900000", emitUF:"SC", emitXLgr:"AV SANTO ANTONIO & CIA", emitNro:"425", emitXCpl:"...", emitBairro:"Centro", emitCMun:"4204608", emitXMun:"Criciúma", remtCnpj:"", 
  remInscricaoEstadual:"ISENTO", remXNome:"CTE EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL", remEmail:"testecte@gmail.com", remFone:"32412046", remCep:"72900000", remUF:"SC", remXLgr:"R: L", remNro:"45", remXCpl:"...",
  remBairro:"Centro", remCMun:"4204608", remXMun:"Criciúma", remCPais:"1058", remXPais:"Brasil", destCnpj:"00456865001139", destCnpjInscricaoEstadual:"ISENTO", destXNome:"CTE EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL",
  destEmail:"testectedest@gmail.com", destFone:"32412046", destCep:"88806620", destUF:"SC", destXLgr:"AV: Líbano", destNro:"789", destXCpl:"...", destBairro:"Santa Luzia", destCMun:"4204608",
  destXMun:"Criciúma", destCPais:"1058", destXPais:"Brasil", vTPrest:"410", vRec:"410", vComp:"500", prestXNome:"Frete Peso", cst:"40", vCarga:"29100.00", proPred:"OLEO DIESEL",
  cUnid:"04", tpMed:"LT", qCarga:"10000.0000", chave:"", RNTRC:"47008950", config:null};
  
  cancelamento = {
    motivo: "",
    protocolo: "",
    chave: ""
  }

  ngOnInit(): void{
  }

  constructor(private http: HttpClient) { }



  emitir(cte: any){   
    let config = localStorage.getItem("config") != null ? JSON.parse(localStorage.getItem("config") || "") : null;  
    if (config != null){
      cte.config = config;
    }else{
      alert("Necessário configurar o emissor.");
      return;
    }
  this.http.post(`${ this.url }/emitir`, cte)
    .subscribe(
      resultado => {
        debugger
        console.log(resultado)
        this.storeAutorizado(resultado);        
      },
      erro => {
        if ((erro.error.text).indexOf("Status:100 - Motivo:Autorizado o uso do CT-e") != -1 ){
         this.storeAutorizado(erro.error.text);
        }else{
          this.storeRejeitado(erro.error.text);
        }
        alert(erro.error.text);
        if(erro.status == 400) {
          console.log(erro);
        }
      }
    );
  }

  storeRejeitado(msg: any){
    let objStorage = localStorage.getItem("cteRejeitado") != null ? JSON.parse(localStorage.getItem("cteRejeitado") || "") : [];
    objStorage.push(JSON.stringify(msg));
    localStorage.setItem("cteRejeitado", JSON.stringify(objStorage));
  }

  storeAutorizado(msg: any){
    let objStorage = localStorage.getItem("cteAutorizado") != null ? JSON.parse(localStorage.getItem("cteAutorizado") || "") : [];
    objStorage.push(JSON.stringify(msg));
    localStorage.setItem("cteAutorizado", JSON.stringify(objStorage));
  }

  cancelar(cancelamento: any){ 
    let config = localStorage.getItem("config") != null ? JSON.parse(localStorage.getItem("config") || "") : null;  
    if (config != null){
      cancelamento.config = config;
    }else{
      alert("Necessário configurar o emissor.");
      return;
    }
    this.http.post<any>(`${ this.url }/cancelar`, cancelamento)
      .subscribe(
        resultado => {
          if (resultado.status == 135 ){
            this.storeCancelamento("Status: " + resultado.status  + " Motivo: " + resultado.motivo + " Chave: " + resultado.chave);
            alert("Status: " + resultado.status + " Motivo: " + resultado.motivo + " Chave: " + resultado.chave);
            cancelamento.motivo = "";
            cancelamento.protocolo = "";
            cancelamento.chave = "";
          }else{
            alert(resultado.motivo);
          }            
          console.log(resultado)       
        }
      );
  }

  storeCancelamento(msg: any){
    let objStorage = localStorage.getItem("cteCancelado") != null ? JSON.parse(localStorage.getItem("cteCancelado") || "") : [];
    objStorage.push(JSON.stringify(msg));
    localStorage.setItem("cteCancelado", JSON.stringify(objStorage));
  }
}
