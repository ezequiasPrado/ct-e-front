import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-nfe',
  standalone: true,
  imports: [HttpClientModule, FormsModule],
  providers:[HttpClientModule, HttpClient, FormsModule],
  templateUrl: './nfe.component.html',
  styleUrl: './nfe.component.css'
})


export class NfeComponent {

  url = 'http://localhost:8080/api/nfe';

  nfe ={ cnpj: "32695077000110", modelo: "55", tipoEmissao:"1",
  tpImp:"1", natOp:"NOTA FISCAL CONSUMIDOR ELETRONICA", numeroNfe:"", serie:"1",
  tpNF:"1", idDest:"1",  codMunFG:"4204608", finNFe:"1", indFinal:"1", indPres:"1",procEmi:"0",
  emitInscricaoEstadual:"258966696", emitXNome:"TRC TRANSPORTES LTDA", emitFone:"6233215175",
  emitCep:"72900000", emitUF:"SC", emitXLgr:"AV SANTO ANTONIO & CIA", emitNro:"425", emitXCpl:"...", emitBairro:"Centro", emitCMun:"4204608", emitXMun:"Criciúma", 
  destCnpj:"00456865001139", indIEDest:"9", destXNome:"NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL",
  destEmail:"testenfedest@gmail.com", destFone:"32412046", destCep:"88806620", destUF:"SC", destXLgr:"AV: Líbano", destNro:"789", destXCpl:"...", destBairro:"Santa Luzia", destCMun:"4204608",
  destXMun:"Criciúma", destCPais:"1058", destXPais:"Brasil", crt:"3",
  vTPrest:"410", vRec:"410", vComp:"500", prestXNome:"Frete Peso", cst:"40", vCarga:"29100.00", proPred:"OLEO DIESEL",
  cUnid:"04", tpMed:"LT", qCarga:"10000.0000", chave:"", RNTRC:"47008950", config: null};

  cancelamento = {
    motivo: "",
    protocolo: "",
    chave: ""
  }
  

  ngOnInit(): void{
  }

  constructor(private http: HttpClient) { }

  emitir(nfe: any){ 
    let config = localStorage.getItem("config") != null ? JSON.parse(localStorage.getItem("config") || "") : null;  
    if (config != null){
      nfe.config = config;
    }else{
      alert("Necessário configurar o emissor.");
      return;
    }
    this.http.post<any>(`${ this.url }/emitir`, nfe)
      .subscribe(
        resultado => {
          debugger
          if ((resultado.motivo).indexOf("Autorizado o uso da NF-e") != -1 ){
            this.storeAutorizado("Status: " + (resultado.status? resultado.status :"") + " Motivo:" + resultado.motivo + " NF-e: " + resultado.numero + " Chave: " + resultado.chave);
           }else{
             this.storeRejeitado("Status: " + (resultado.status? resultado.status :"") + " Motivo:" + resultado.motivo + " NF-e: " + resultado.numero + " Chave: " + resultado.chave);
           }
           alert("Status: " + (resultado.status? resultado.status :"") + " Motivo:" + resultado.motivo + " NF-e: " + resultado.numero + " Chave: " + resultado.chave);
          console.log(resultado)       
        },
        erro => {
          debugger
          if ((erro.error.text).indexOf("Autorizado o uso da NF-e") != -1 ){
           this.storeAutorizado(erro.error.text);
          }else{
            this.storeRejeitado(erro.error.text);
          }
          alert(erro.error.text);
          if(erro.status == 400) {
            console.log(erro);
          }
        }
      );
    }
  
    storeRejeitado(msg: any){
      let objStorage = localStorage.getItem("nfeRejeitado") != null ? JSON.parse(localStorage.getItem("nfeRejeitado") || "") : [];
      objStorage.push(JSON.stringify(msg));
      localStorage.setItem("nfeRejeitado", JSON.stringify(objStorage));
    }
  
    storeAutorizado(msg: any){
      let objStorage = localStorage.getItem("nfeAutorizado") != null ? JSON.parse(localStorage.getItem("nfeAutorizado") || "") : [];
      objStorage.push(JSON.stringify(msg));
      localStorage.setItem("nfeAutorizado", JSON.stringify(objStorage));
    }

    cancelar(cancelamento: any){ 
      let config = localStorage.getItem("config") != null ? JSON.parse(localStorage.getItem("config") || "") : null;  
      if (config != null){
        cancelamento.config = config;
      }else{
        alert("Necessário configurar o emissor.");
        return;
      }
      this.http.post<any>(`${ this.url }/cancelar`, cancelamento)
        .subscribe(
          resultado => {
            if (resultado.status == 135 ){
              this.storeCancelamento("Status: " + resultado.status + " Motivo:" + resultado.motivo + " Protocolo: " + resultado.protocolo + " Chave: " + resultado.chave);
              alert("Status: " + (resultado.status? resultado.status :"") + " Motivo:" + resultado.motivo + " Protocolo: " + resultado.protocolo + " Chave: " + resultado.chave);
              cancelamento.motivo = "";
              cancelamento.protocolo = "";
              cancelamento.chave = "";
            }else{
              alert(resultado.motivo);
            }            
            console.log(resultado)       
          }
        );
      }

      storeCancelamento(msg: any){
        let objStorage = localStorage.getItem("nfeCancelado") != null ? JSON.parse(localStorage.getItem("nfeCancelado") || "") : [];
        objStorage.push(JSON.stringify(msg));
        localStorage.setItem("nfeCancelado", JSON.stringify(objStorage));
      }

}
